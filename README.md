# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init
git remote add origin git@bitbucket.org:vehar654/stroboskop.git
git pull origin master
```

Naloga 6.2.3:

https://bitbucket.org/vehar654/stroboskop/commits/bfb9d283decbcb5c2464caae74ddf04b3b82d881

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/vehar654/stroboskop/commits/52a25dd76ec4c1f4f051ae390ed8d0992516af2a

Naloga 6.3.2:

https://bitbucket.org/vehar654/stroboskop/commits/e144662af7b09d97919de03fde2021e1c3911a11

Naloga 6.3.3:

https://bitbucket.org/vehar654/stroboskop/commits/a823d4a3b4c74d24b13f0b3122ac99b98ce7d5b0

Naloga 6.3.4:

https://bitbucket.org/vehar654/stroboskop/commits/818876873bc9f34fcb8a20ed46472b2a5b9d80a3

Naloga 6.3.5:

```
git checkout master
git merge izgled
git add *
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/vehar654/stroboskop/commits/d863d6b194cf4ee2656a29a7fddf78b70b242833

Naloga 6.4.2:

https://bitbucket.org/vehar654/stroboskop/commits/ce7c2f8d0a3c5218315e5b027f13777aba0efb89

Naloga 6.4.3:

https://bitbucket.org/vehar654/stroboskop/commits/7525465757eb6412aa0346bae70d717bf819330b

Naloga 6.4.4:

https://bitbucket.org/vehar654/stroboskop/commits/beb1086206750a0e3cf1825d8a738fa4a79c2e0f